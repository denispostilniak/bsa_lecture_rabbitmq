﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScope
    {
        IMessageConsumer MessageConsumer { get; }
    }
}
