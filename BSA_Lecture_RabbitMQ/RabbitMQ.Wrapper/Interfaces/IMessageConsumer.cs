﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Received;
        void Connect();
        void SetAcknowledge(ulong deliveryTag, bool processed);
    }
}
