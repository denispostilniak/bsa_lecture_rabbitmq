﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Services
{
    public class ConnectionFactory
    {
        public IConnection CreateConnection(string hostName)
        {
            return new Client.ConnectionFactory
            {
                HostName = hostName
            }.CreateConnection();
        }
    }
}
